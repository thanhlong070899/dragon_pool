const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const faqSchema = new mongoose.Schema({
    Faq_ID: {
        type: Number,
        unique: [true, "Faq ID is taken."],
        required: true
    },
    Faq_Order:{
        type: Number,
        default: null
    },
    Faq_Title:{
        type: String,
        required: [true, "Faq_Title is required."]
    },
    Faq_Content: {
        type: String,
        required: [true, "Faq_Content is required."]
    },
    Faq_Deleted:{
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('faq', faqSchema)