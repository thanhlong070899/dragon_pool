const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const memberSchema = new mongoose.Schema({
    Member_ID: {
        type: Number,
        unique: [true, "Partner ID is taken."],
        required: true
    },
    Member_Name_en: {
        type: String,
        required: true
    },
    Member_Name_vi: {
        type: String,
        default: ""
    },
    Member_Description_en: {
        type: String,
        default: ""
    },
    Member_Description_vi: {
        type: String,
        default: ""
    },
    Member_Content_en: {
        type: String,
        default: ""
    },
    Member_Content_vi: {
        type: String,
        default: ""
    },
    Member_Avatar: {
        type: String,
        default: ""
    },
    Member_Priority: {
        type: Number,
        default: 0
    },
    Member_Show_en: {
        // true => show , false => hide
        type: Boolean,
        required: true,
        default: false
    },
    Member_Show_vi: {
        // true => show , false => hide
        type: Boolean,
        required: true,
        default: false
    },
    Member_Deleted:{
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('member', memberSchema)