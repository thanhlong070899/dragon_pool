const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const airdropSchema = new mongoose.Schema({
    Airdrop_ID: {
        type: Number,
        unique: [true, "Airdrop ID is taken."],
        required: true
    },
    Airdrop_Username:{
        type: String,
        default: null
    },
    Airdrop_ID:{
        type: Number,
        required: true
    },
    Airdrop_BscAddress:{
        type: String,
        default : null
    },
    Airdrop_Email:{
        type: String,
        default: null,
    },
    Airdrop_Deleted:{
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('airdrop', airdropSchema)