const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const missionSchema = new mongoose.Schema({
    Mission_ID: {
        type: Number,
        unique: [true, "Mission ID is taken."],
        required: true
    },
    Mission_Name: {
        type: String,
        required: true        
    },
    Mission_Decription: {
        type: String,
    },
    Mission_Deleted: {
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('mission', missionSchema)