const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const postSchema = new mongoose.Schema({
    Post_ID:{
        type: Number,
        unique: [true, "Partner ID is taken."],
        required: true
    },
    Post_By:{
        type: Number,
        default: 0
    },
    Post_Title_en:{
        type: String,
        required:[true,"Post_Title_en is required." ]
    },
    Post_Title_vi:{
        type: String,
        default: ""
    },
    Post_Description_en:{
        type: String,
        default: ""
    },
    Post_Description_vi:{
        type: String,
        default: ""
    },
    Post_Keywords_en:{
        type: String,
        default: ""
    },
    Post_Keywords_vi:{
        type: String,
        default: ""
    },
    Post_Content_en:{
        type: String,
        default: ""
    },
    Post_Content_vi:{
        type: String,
        default: ""
    },
    Post_View_en:{
        type: String,
        default: ""
    },
    Post_View_vi:{
        type: String,
        default: ""
    },
    Post_CoverImg:{
        type: String,
        default: ""
    },
    Post_Created:{
        type: Number,
        default: new Date().getTime()
    },
    Post_Updated:{
        type: Number,
        default: new Date().getTime()
    },
    Post_Priority:{
        type: Number,
        default: null
    },
    Post_Show_en:{
        // true => show , false => hide
        type: Boolean,
        default: false
    },
    Post_Show_vi:{
        // true => show , false => hide
        type: Boolean,
        default: false
    },
    Post_Hot:{
        // true => higlight , false => none
        type: Boolean,
        default: false
    },
    Post_Deleted:{
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('post', postSchema)
