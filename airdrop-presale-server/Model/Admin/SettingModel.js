const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const settingSchema = new mongoose.Schema({
    Setting_LangName: {
        type: String,
        default: "English",
        required: true
    },
    Setting_Active: {
        type: Boolean,
        default: 0
    },
    Setting_Default: {
        type: Boolean,
        default: 0
    },
    Setting_Order: {
        type: Number,
        default: 0
    },
    Setting_Countries: {
        type: String,
        default: ""
    },
    Setting_Title: { // co
        type: String,
        required: [true, "title is required."]
    },
    Setting_Slogan: { // co
        type: String
    },
    Setting_Description: { // co
        type: String,
        required: [true, "description is required."]
    },
    Setting_Keywords: { // co
        type: String,
        default: ""
    },
    Setting_CompanyID: { // co
        type: String,
        default: "",
    },
    Setting_CompanyName: { // co
        type: String,
        default: "",
    },
    Setting_Phone: { // co
        type: String,
        default:"",
    },
    Setting_Email: { // co
        type: String,
        default: "",
    },
    Setting_Address: { // co
        type: String,
        default: "",
    },
    Setting_GoogleMapLink: { // co
        type: String,
        default: ""
    },
    Setting_GoogleMapsEmbed: { // co
        type: String,
        default: ""
    },
    Setting_SocialFacebook: { // co
        type: String,
        default: ""
    },
    Setting_SocialInstagram: { // co
        type: String,
        default: ""
    },
    Setting_SocialYoutube: { // co
        type: String,
        default: ""
    },
    Setting_SocialZalo: { // co
        type: String,
        default: ""
    },
    Setting_DateFormat: {
        type: String,
        default: "d/m/y"
    },
    Setting_TimeFormat: {
        type: String,
        default: "H:i:s"
    },
    Setting_NumberGrouping: {
        type: String,
        default: ""
    },
    Setting_NumberDecimal: {
        type: String,
    },
    Setting_CurrencyDecimals: {
        type: Number
    },
    Setting_TelegramWelcome: {
        type: String,
        required: true
    },
    Setting_TelegramMobileRequire: {
        type: String,
        required: true
    },
    Setting_TelegramEmailRequire: {
        type: String,
        required: true
    },
    Setting_TelegramBSCAddressRequire: {
        type: String,
        required: true
    },
    Setting_TelegramVipRequire: {
        type: String,
        required: true
    },
    Setting_TelegramBank1: {
        type: String,
        required: true
    },
    Setting_TelegramBank2: {
        type: String,
        required: true
    },
    Setting_TelegramBank3: {
        type: String,
        required: true
    },
    Setting_TelegramVipMessage: {
        type: String,
        required: true
    },
    Setting_TelegramVipAccount: {
        type: String,
        required: true
    },
    Setting_IBWelcome: {
        type: String,
        required: true
    },
    Setting_IBInstruct: {
        type: String,
        required: true
    },
    Setting_ExpertWelcome: {
        type: String,
        required: true
    },
    Setting_ExpertInstruct: {
        type: String,
        required: true
    },
    Setting_IntroductionManaBot: {
        type: String,
        required: true
    },
    Setting_SignalExtension: {
        type: String,
        required: true
    },
    Setting_TelegramUserNotifys: {
        type: String,
        required: true
    },
    Setting_Lang: {
        type: String,
        required: true
    },
    Setting_Deleted:{
        type: Boolean,
        default: false
    },

},{timestamps: true})

module.exports = connect.admin_DB.model('setting', settingSchema)
