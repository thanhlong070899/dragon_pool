const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const permissionSchema = new mongoose.Schema({
    Permission_ID: {
        type: Number,
        unique: [true, "Permission ID is taken."],
        required: true
    },
    Permission_Name:{
        type: String,
        default: ""
    },
    Permission_Deleted:{
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('permission', permissionSchema)