const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const countrySchema = new mongoose.Schema({
    Country_Name: {
        type: String,
        required: true
    },
    Country_Code:{
        type: Number,
        default: 0,
        unique: [true, "Country Code is taken."],
    },
    Country_ID:{
        type: String,
        default: "",
        unique: [true, "Country ID is taken."],
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('country', countrySchema)