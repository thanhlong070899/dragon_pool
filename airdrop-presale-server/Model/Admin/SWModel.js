const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')
const moment = require("moment")
const FULL_DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"


const swSchema = new mongoose.Schema({
    Sw_ID: {
        type: Number,
        unique: [true, "Sw_ID is taken."],
        required: true
    },
    Sw_Name:{
        type: String,
        default: ""
    },
    Sw_Email:{
        type: String,
        default: ""
    },
    Sw_Birthday:{
        type: Date,
        default: moment.utc(Date.now()).format(FULL_DATE_FORMAT)
    },
    Sw_EntryType:{
        type: String,
    },
    Sw_Action:{
        type: String,
        default: ""
    },
    Sw_Details:{
        type: String,
        default: ""
    },
    Sw_EntriesWorth:{
        type: String,
        default: ""
    },
    Sw_Created:{
        type: Number,
        default: new Date().getTime()
    },
    Sw_Country:{
        type: String,
        default:""
    },
    Sw_Status:{
        type: Boolean,
        default: false
    },
    Sw_Deleted:{
        type: Boolean,
        default: false
    }

}, { timestamps: true })

module.exports = connect.admin_DB.model('sw', swSchema)