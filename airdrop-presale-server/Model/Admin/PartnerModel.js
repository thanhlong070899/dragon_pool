const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')
const moment = require("moment")
const FULL_DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"

const partnerSchema = new mongoose.Schema({
    Partner_ID: {
        type: Number,
        unique: [true, "Partner ID is taken."],
        required: true
    },
    Partner_Name_en: {
        type: String,
        required: true
    },
    Partner_Name_vi: {
        type: String,
        default: ""
    },
    Partner_Description_en: {
        type: String,
        default: ""
    },
    Partner_Description_vi: {
        type: String,
        default: ""
    },
    Partner_Img: {
        type: String,
        default: ""
    },
    Partner_Link: {
        type: String,
        default: ""
    },
    Partner_Target: {
        //self=>Cửa sổ hiện tại||_blank=>Cửa sổ mới	
        type: String,
        enum: ["_self", "_blank"],
        default: "_self"
    },
    Partner_Show_en: {
        // true => show , false => hide
        type: Boolean,
        required: true,
        default: false
    },
    Partner_Order: {
        type: Number,
        default: 0
    },
    Partner_Created: {
        type: Number,
        default: new Date().getTime()
    },
    Partner_Updated: {
        type: Number,
        default: new Date().getTime()
    },
    Partner_Deleted:{
        type: Boolean,
        default: false
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('partner', partnerSchema)
