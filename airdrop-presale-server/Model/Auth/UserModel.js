const mongoose = require('mongoose')

const connect = require('../../Config/ConnectDB')

const userSchema = new mongoose.Schema({
    User_ID: {
        type: Number,
        required: true,
        unique: [true, "User ID is taken."],
    },
    User_Avatar: {
        type: String,
        default: ""
    },
    User_DocumentPhotos: {
        type: String,
        default: null
    },
    User_Name: {
        type: String,
        default: null
    },
    User_FirstName: {
        type: String,
    },
    User_LastName: {
        type: String,
    },
    User_Gender: {
        type: String,
        default: "",
        enum:["Male", "Female", "Orther","Nam","Nu", "Khac", ""]
    },
    User_Birthday: {
        type: Date,
        default: ""
    },
    User_Country: {
        type: String,
        default: ""
    },
    User_Email: {
        type: String,
        required: true,
    },
    User_EmailVerified: {
        type: Number,
        default: 0
    },
    User_Mobile: {
        type: String,
        default: ""
    },
    User_MobileVerified: {
        type: Number,
        default: 0
    },
    User_Address: {
        type: String,
        default: ""
    },
    User_BSCAddress: {
        type: String,
    },
    User_Group: {
        type: String,
        default: ""
    },
    User_Permissions: {
        type: Array,
        default: []
    },
    User_Password: {
        type: String,
        required: true,
    },
    User_Registered: {
        type: Number,
        default: new Date().getTime()
    },
    User_SessionIDs: {
        type: String,
        default: ""
    },
    User_RootAdmin: {
        type: Number,
        default: 0
    },
    User_VipExpirationDate: {
        type: Number,
        default: 0
    },
    User_Read: {
        type: Number,
        default: 0
    },
    User_Status: {
        type: Boolean,
        default: false
    },
    User_Salt: {
        type: String,
        default: ""
    },
    User_Token: {
        type: String,
        default: ""
    },
    User_Deleted: {
        type: Boolean,
        default: false
    },
}, { timestamps: true })

module.exports = connect.admin_DB.model('user', userSchema)