const UserModel = require('../../Model/Auth/UserModel')
const {encrypt, decrypt} = require('../../Hepper/Xxtea')
const {generateJWT,verifyJWT} = require('../../Middleware/AuthHandle')
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const {randomString} = require('../../Hepper/RandomString')

const userLogin = async (req, res) => {
    try {
        const { email, password } = req.body
        var user = await UserModel.findOne({ User_Email: email, User_Deleted: false }).select("+User_Token")
        if (!user)
            return res.status(200).json({ status: false, message: "user is not found" })
        var check_password = await decrypt(user.User_Password)
        if (!check_password.status)
            return res.status(200).json(check_password)
        if (check_password.data != password)
            return res.status(200).json({ status: false, message:"password is not correct" })
        if (!user.User_Token) {
            user.User_Token = generateJWT({ User_ID: user.User_ID })
            await user.save()
        } else {
            // Existed but expired.
            try {
                verifyJWT(user.User_Token)
            } catch (err) {
                if (err.name === "TokenExpiredError") {
                    user.User_Token = await generateJWT({ User_ID: user.User_ID })
                    await user.save()
                }
            }
        }
        return res.status(200).json({ status: true, data: {email: user.User_Email,token:user.User_Token }, message:"Login success"})
    } catch (error) {
        return res.status(200).json({ status: false, error: error.message })
    }
}

const userChangePassword = async(req,res) =>{
    try {
        const {current_password, new_password} = req.body

        if (!current_password || current_password == null) return res.status(200).json({ status: false, message: "Input current password please" })
        if (!new_password || new_password == null) return res.status(200).json({ status: false, message: "Input new password please" })
        const user = await UserModel.findOne({ User_ID: req.user.User_ID, User_Deleted: false })
        if(!user) return res.status(200).json({status: false, message: "User is not found"})
        var password = (await decrypt(user.User_Password)).data
        if(password != current_password) return res.status(200).json({status: false, message: "Current password is not match"})
        if(password == new_password) return res.status(200).json({status: false, message: "Current and new password are the same"})
        user.User_Password = (await encrypt(new_password)).data
        await user.save()
        return res.status(200).json({status: true, message: "Change password success"})
    } catch (error) {
        return res.status(200).json({ status: false, message: error.message })
    }
}

const userLogout = async (req, res) => {
    try {
        await UserModel.updateOne({ User_ID: req.user.User_ID }, { User_Token: null })
        return res.status(200).json({ status: true, message: "Success" })
    } catch (error) {
        return res.status(200).json({ status: false, message: error.message })
    }
}

const sendEmail = async (req, res) =>{
    try {
        const { User_Email } = req.body
        var user = await UserModel.findOne({ User_Email: User_Email })
        if (!user)
            return res.status(200).json({ status: false, error: `Email ${User_Email} is not found` })
        var secrect_key = await randomString()
        user.User_Secrect_Key = secrect_key
         await user.save()
        // user.save()
        var transporter =  nodemailer.createTransport({ // config mail server
            service: 'Gmail',
            auth: {
                user: 'longsuy0@gmail.com',
                pass: 'pvdmlktdyunbwlpc'
            }
        });
      
        // send mail with defined transport object
        var mainOptions = { // thiết lập đối tượng, nội dung gửi mail
            from: 'Thanh Long',
            to: User_Email,
            subject: 'Forgot password',
            text: 'You recieved message from dragon pool' ,
            html: `Your secrect key : <b>${secrect_key}</b>`
        }
      
        transporter.sendMail(mainOptions, function(err, info){
            if (err) {
                console.log(err);
                res.redirect('/');
            } else {
                console.log('Message sent: ' +  info.response);
                res.redirect('/');
            }
        });
        return {status:true, message: "Secrect key sent to your email"}
    } catch (error) {
        console.log(error)
        return { status: false, error: error.message }
    }
}
const verifyEmail = async (req, res) => {
    try {
        const { User_Email, User_Secrect_Key } = req.body
        if (User_Email == undefined || User_Email == "" || User_Email == null)
            return res.status(200).json({ status: false, message: `User_Email is required` })
        var user = await UserModel.findOne({ User_Email: User_Email })
        if (!user)
            return res.status(200).json({ status: false, error: `${User_Email} is not found` })
        if (user.User_Secrect_Key != User_Secrect_Key)
            return res.status(200).json({ status: false, error: `Secrect Key is not correct` })
        return res.status(200).json({ status: true, message: `Secrect Key is correct` })
    } catch (error) {
        return { status: false, error: error.message }
    }
}

const resetPassword = async (req, res) => {
    try {
        const { User_Email, User_NewPassword } = req.body
        if (User_Email == undefined || User_Email == "" || User_Email == null)
            return res.status(200).json({ status: false, message: `User_Email is required` })
        if (User_NewPassword == undefined || User_NewPassword == "" || User_NewPassword == null)
            return res.status(200).json({ status: false, message: `User_NewPassword is required` })
        var user = await UserModel.findOne({ User_Email: User_Email })
        if (!user)
            return res.status(200).json({ status: false, error: `${User_Email} is not found` })
        user.User_Password = (await encrypt(User_NewPassword)).data
        user.User_Secrect_Key = null
        await user.save()
        return res.status(200).json({ status: true, message: "Reset password success" })
    } catch (error) {
        console.log(error)
        return { status: false, error: error.message }
    }
}

module.exports = {userLogin, userLogout, userChangePassword, verifyEmail, resetPassword, sendEmail}