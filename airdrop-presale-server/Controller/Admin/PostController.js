const PostModel = require('../../Model/Admin/PostModel')
const UserModel = require('../../Model/Auth/UserModel')
const moment = require("moment")
const FULL_DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"

const postCreate = async (req, res) => {
    try {
        const { Post_Title_en, Post_Description_en, Post_Keywords_en, Post_Content_en, Post_View_en, Post_CoverImg, Post_Created, Post_Updated, Post_Priority, Post_Show_en, Post_By, Post_Hot } = req.body
        var data = { Post_Title_en, Post_Description_en, Post_Keywords_en, Post_Content_en, Post_View_en, Post_CoverImg, Post_Created, Post_Updated, Post_Priority, Post_Show_en, Post_By, Post_Hot }
        const { errors } = await postValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
            if(Post_By)
        data.Post_Created = new Date(Post_Created)
        data.Post_Updated = new Date(Post_Updated)
        if(Post_By != undefined || Post_By != null)
        {
            var check_post_by = await UserModel.findOne({ User_ID: Post_By })
            if(!check_post_by)
            return res.status(200).json({status: false , error: `Post_By ${Post_By} is not found`})
        }
        
        var lastest_post = await PostModel.find().sort({ Post_ID: -1 }).limit(1)
        lastest_post.length > 0 ? data.Post_ID = lastest_post[0].Post_ID : data.Post_ID = 1
        var test = await new PostModel.save(data)
        return res.status(200).json({ status: true, message: "Create post success" })
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }

}

const postEdit = async (req, res) => {
    try {
        const { Post_ID, Post_Title_en, Post_Description_en, Post_Keywords_en, Post_Content_en, Post_View_en, Post_CoverImg, Post_Created, Post_Updated, Post_Priority, Post_Show_en, Post_By, Post_Hot } = req.body
        var data = { Post_Title_en, Post_Description_en, Post_Keywords_en, Post_Content_en, Post_View_en, Post_CoverImg, Post_Created, Post_Updated, Post_Priority, Post_Show_en, Post_By, Post_Hot }
        if (Post_ID == undefined || Post_ID == null || Post_ID == "")
            return res.status(200).json({ status: false, message: "Post_ID is required." })
        var post = await PostModel.findOne({ Post_ID: Post_ID })
        if (!post)
            return res.status(200).json({ status: false, message: `Post_ID ${Post_ID} is not found.` })
        const { errors } = await postValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        data.Post_Created = new Date(Post_Created)
        data.Post_Updated = new Date(Post_Updated)
        PostModel.updateOne({Post_ID:Post_ID}, data)
        return res.status(200).json({ status: true, message: "Create post success" })
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }
}
const postDelete = async (req, res) => {
    const { Post_ID } = req.body
    var message = []
    if (!Array.isArray(Post_ID) || Post_ID.length <= 0)
        return res.status(200).json({ status: false, message: "Post_ID is not correct format." })
    for (var i = 0; i < Post_ID.length; i++) {
        if(!isNaN(Post_ID[i]))
        var Post = await PostModel.findOne({ Post_ID: Post_ID[i], Post_Deleted: false })
        if (!Post)
            message.push({ status: false, message: `ID ${Post_ID[i]} is not found` })
        else {
            Post.Post_Deleted = true
            Post.save()
            message.push({ status: true, message: `Delete ID ${Post_ID[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}
const postFilter = async (req, res) => {

    try {
        var list_data = []
        var perPage = 5; // số lượng sản phẩm xuất hiện trên 1 page
        var page = req.body.page || 1;
        await PostModel.find({ Post_Deleted: false }).select("-_id -createdAt -updatedAt ")
            .skip((perPage * page) - perPage).limit(perPage)
            .exec(async (err, data) => {
                list_data = data
                var total_data = await PostModel.find({ Post_Deleted: false }).countDocuments()
                return res.status(200).json({ status: true, total_data: total_data, data_page: data.length, data: list_data, perPage: perPage, page: page })
            });
    } catch (error) {
        return { status: false, message: error.message }
    }
}

const postDetail = async (req, res) => {
    try {
        const { Post_ID } = req.params
        if (Post_ID == "" || Post_ID == null || Post_ID == undefined)
            return res.status(200).json({ status: false, message: "input Post_ID please" })
        var post = await PostModel.findOne({ Post_ID: Post_ID, Post_Deleted: false }).select("-_id -createdAt -updatedAt ")
        if (!post)
            return res.status(200).json({ status: false, message: `Post ${Post_ID} is not found` })
        return res.status(200).json({ status: true, data: post }) 
    } catch (error) {
        return res.status(200).json({status: false, message: error.message})
    }

}

const postValidate = async (data) => {
    var errors = []
    if(data.Post_Title_en == undefined || data.Post_Title_en == null || data.Post_Title_en == "")
    errors.push({field:"Post_Title_en", error: "Post_Title_en is required."})
    if (data.Post_Priority == undefined && data.Post_Priority == null && isNaN(data.Post_Priority))
    errors.push({field:"Post_Priority", error: "Post_Priority is not correct format."})
    return { errors: errors }
}

module.exports = { postCreate, postEdit, postDelete, postFilter, postDetail }
