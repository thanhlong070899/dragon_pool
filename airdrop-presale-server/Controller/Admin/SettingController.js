const SettingModel = require('../../Model/Admin/SettingModel')

const createSetting = async (req, res) => {
    try {
        data = req.body
        const { errors } = editValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        var check_setting = await SettingModel.findOne({ Setting_Lang: data.Setting_Lang })
        if (check_setting)
            return res.status(200).json({ status: false, message: `setting ${data.Setting_Lang} is existed` })
        var test = await new SettingModel(data).save()
        return res.status(200).json({ status: true, message: `create setting ${data.Setting_Lang} success. ` })
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }
}

const editSetting = async (req, res) => {
    try {
        data = req.body
        if (data.Setting_Lang == undefined || data.Setting_Lang == "" || data.Setting_Lang == null)
            return res.status(200).json({ status: false, message: "Setting_Lang is required" })
        var setting = await SettingModel.findOne({ Setting_Lang: data.Setting_Lang, Setting_Deleted: false })
        if (!setting)
            return res.status(200).json({ status: false, message: `Setting ${data.Setting_Lang} is not found` })
        const { errors } = editValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        var test = await SettingModel.updateOne({Setting_Lang:data.Setting_Lang},data)
        return res.status(200).json({ status: false, message: `edit setting user ${req.user.User_ID}  success. ` })
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }
}

const loadSetting = async (req, res) => {
    try {
        var list_data = []
        var perPage = 5; // số lượng sản phẩm xuất hiện trên 1 page
        var page = req.body.page || 1;
        await SettingModel.find({ Setting_Deleted: false }).select("-_id -createdAt -updatedAt ")
            .skip((perPage * page) - perPage).limit(perPage)
            .exec(async (err, data) => {
                list_data = data
                var total_data = await SettingModel.find({ Setting_Deleted: false }).countDocuments();
                return res.status(200).json({ status: true, data_page: data.length, data: list_data, perPage: perPage, page: page, total_data: total_data })
            });
    } catch (error) {
        console.log(error)
        return { status: false, message: error.message }
    }
}

const detailSetting = async (req, res) => {
    try {
        const { Setting_Lang } = req.params
        if (Setting_Lang == undefined || Setting_Lang == null || Setting_Lang == "")
            return res.status(200).json({ status: false, message: "Setting_Lang is required." })
        var setting = await SettingModel.findOne({ Setting_Lang: Setting_Lang, Setting_Deleted: false })
        if (!setting)
            return res.status(200).json({ status: false, message: `Setting ${Setting_Lang} is not found` })
        return res.status(200).json({ status: true, data: setting })
    } catch (error) {
        return res.status(200).json({ status: false, message: error.message })
    }
}

const deleteSetting = async (req, res) => {
    const { Setting_Lang } = req.body
    var message = []
    if (!Array.isArray(Setting_Lang) || Setting_Lang.length <= 0)
        return res.status(200).json({ status: false, message: "Setting_Lang is not correct format." })
    for (var i = 0; i < Setting_Lang.length; i++) {
        if(typeof Setting_Lang[i] != 'string')
        var Setting = await SettingModel.findOne({ Setting_Lang: Setting_Lang[i], Setting_Deleted: false })
        if (!Setting)
            message.push({ status: false, message: `ID ${Setting_Lang[i]} is not found` })
        else {
            Setting.Setting_Deleted = true
            Setting.save()
            message.push({ status: true, message: `Delete ID ${Setting_Lang[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}

const editValidate = (data) => {
    var errors = []
    try {
        if (data.Setting_Title == undefined || data.Setting_Title == null || data.Setting_Title == "")
            errors.push({ field: "Setting_Title", error: "Setting_Title is required" })
        if (data.Setting_LangName == undefined || data.Setting_LangName == null || data.Setting_LangName == "")
            errors.push({ field: "Setting_LangName", error: "Setting_LangName is required" })
        if (data.Setting_Description == undefined || data.Setting_Description == null || data.Setting_Description == "")
            errors.push({ field: "Setting_Description", error: "Setting_Description is required" })
        if (data.Setting_Order != undefined && isNaN(data.Setting_Order))
            errors.push({ field: "Setting_Order", error: "Setting_Order is not correct format" })
        if (data.Setting_Default != undefined && isNaN(data.Setting_Default))
            errors.push({ field: "Setting_Default", error: "Setting_Default is not correct format" })
        if (data.Setting_DateFormat == undefined || data.Setting_DateFormat == null || data.Setting_DateFormat == "")
            errors.push({ field: "Setting_DateFormat", error: "Setting_DateFormat is required" })
        if (data.Setting_TimeFormat == undefined || data.Setting_TimeFormat == null || data.Setting_TimeFormat == "")
            errors.push({ field: "Setting_TimeFormat", error: "Setting_TimeFormat is required" })
        if (data.Setting_NumberGrouping == undefined && isNaN(data.Setting_NumberGrouping))
            errors.push({ field: "Setting_NumberGrouping", error: "Setting_NumberGrouping is not correct format" })
        if (data.Setting_NumberDecimal == undefined && isNaN(data.Setting_NumberDecimal))
            errors.push({ field: "Setting_NumberDecimal", error: "Setting_NumberDecimal is not correct format" })
        if (data.Setting_CurrencyDecimals == undefined && isNaN(data.Setting_CurrencyDecimals))
            errors.push({ field: "Setting_CurrencyDecimals", error: "Setting_CurrencyDecimals is not correct format" })
        if (data.Setting_TelegramWelcome == undefined || data.Setting_TelegramWelcome == null || data.Setting_TelegramWelcome == "")
            errors.push({ field: "Setting_TelegramWelcome", error: "Setting_TelegramWelcome is required" })
        if (data.Setting_TelegramMobileRequire == undefined || data.Setting_TelegramMobileRequire == null || data.Setting_TelegramMobileRequire == "")
            errors.push({ field: "Setting_TelegramMobileRequire", error: "Setting_TelegramMobileRequire is required" })
        if (data.Setting_TelegramEmailRequire == undefined || data.Setting_TelegramEmailRequire == null || data.Setting_TelegramEmailRequire == "")
            errors.push({ field: "Setting_TelegramEmailRequire", error: "Setting_TelegramEmailRequire is required" })
        if (data.Setting_TelegramBSCAddressRequire == undefined || data.Setting_TelegramBSCAddressRequire == null || data.Setting_TelegramBSCAddressRequire == "")
            errors.push({ field: "Setting_TelegramBSCAddressRequire", error: "Setting_TelegramBSCAddressRequire is required" })
        if (data.Setting_TelegramVipRequire == undefined || data.Setting_TelegramVipRequire == null || data.Setting_TelegramVipRequire == "")
            errors.push({ field: "Setting_TelegramVipRequire", error: "Setting_TelegramVipRequire  is required" })
        if (data.Setting_TelegramBank1 == undefined || data.Setting_TelegramBank1 == null || data.Setting_TelegramBank1 == "")
            errors.push({ field: "Setting_TelegramBank1", error: "Setting_TelegramBank1 is required" })
        if (data.Setting_TelegramBank2 == undefined || data.Setting_TelegramBank2 == null || data.Setting_TelegramBank2 == "")
            errors.push({ field: "Setting_TelegramBank2", error: "Setting_TelegramBank2 is required" })
        if (data.Setting_TelegramBank3 == undefined || data.Setting_TelegramBank3 == null || data.Setting_TelegramBank3 == "")
            errors.push({ field: "Setting_TelegramBank3", error: "Setting_TelegramBank3 is required" })
        if (data.Setting_TelegramVipMessage == undefined || data.Setting_TelegramVipMessage == null || data.Setting_TelegramVipMessage == "")
            errors.push({ field: "Setting_TelegramVipMessage", error: "Setting_TelegramVipMessage is required" })
        if (data.Setting_IBWelcome == undefined || data.Setting_IBWelcome == null || data.Setting_IBWelcome == "")
            errors.push({ field: "Setting_IBWelcome", error: "Setting_IBWelcome is required" })
        if (data.Setting_IBInstruct == undefined || data.Setting_IBInstruct == null || data.Setting_IBInstruct == "")
            errors.push({ field: "Setting_IBInstruct", error: "Setting_IBInstruct  is required" })
        if (data.Setting_ExpertWelcome == undefined || data.Setting_ExpertWelcome == null || data.Setting_ExpertWelcome == "")
            errors.push({ field: "Setting_ExpertWelcome", error: "Setting_ExpertWelcome  is required" })
        if (data.Setting_ExpertInstruct == undefined || data.Setting_ExpertInstruct == null || data.Setting_ExpertInstruct == "")
            errors.push({ field: "Setting_ExpertInstruct", error: "Setting_ExpertInstruct  is required" })
        if (data.Setting_IntroductionManaBot == undefined || data.Setting_IntroductionManaBot == null || data.Setting_IntroductionManaBot == "")
            errors.push({ field: "Setting_IntroductionManaBot", error: "Setting_IntroductionManaBot  is required" })
        if (data.Setting_TelegramUserNotifys == undefined || data.Setting_TelegramUserNotifys == null || data.Setting_TelegramUserNotifys == "")
            errors.push({ field: "Setting_TelegramUserNotifys", error: "Setting_TelegramUserNotifys  is required" })
        if (data.Setting_SignalExtension == undefined || data.Setting_SignalExtension == null || data.Setting_SignalExtension == "")
            errors.push({ field: "Setting_SignalExtension", error: "Setting_SignalExtension  is required" })
        if (data.Setting_Lang == undefined || data.Setting_Lang == null || data.Setting_Lang == "")
            errors.push({ field: "Setting_Lang", error: "Setting_Lang is required" })
        return { errors: errors }
    } catch (error) {
        errors.push({ field: "error", error: error.message })
        return { errors }
    }
}

module.exports = { editSetting, createSetting, loadSetting, deleteSetting, detailSetting }