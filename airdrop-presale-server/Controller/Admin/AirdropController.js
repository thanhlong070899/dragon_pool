const SwModel = require('../../Model/Admin/SWModel')
const AirdropModel = require('../../Model/Admin/AirdropModel')
const MissionModel = require('../../Model/Admin/MissionModel')

var json_data = []

const selectData = async (req, res) => {
    const { Airdrop_Mission, Airdrop_Email, Airdrop_BSCAddress } = req.body
    var errors = []
    var mission_data = []
    if (!Array.isArray(Airdrop_Mission))
        return res.status(200).json({ status: false, message: "Airdrop mission is not correct" })
    for (var index = 0; index < Airdrop_Mission.length; index++) {
        if (isNaN(Airdrop_Mission[i]))
            errors.push({ field: `Mission ID ${Airdrop_Mission[i]} is not correct` })
        else {
            var check_mission = await MissionModel.findOne({ Mission_ID: Airdrop_Mission[i] })
            if (!check_mission)
                errors.push({ field: `Mission ID ${Airdrop_Mission[i]} is not found` })
            else
                mission_data.push({ Mission_ID: check_mission.Mission_ID, Mission_Name: check_mission.Mission_Name })
        }
    }
    if (errors.length > 0)
        return res.status(200).json({ status: false, message: errors })
    if (typeof Airdrop_Email != 'boolean')
        return res.status(200).json({ status: false, message: `Email status is not correct` })
    if (typeof Airdrop_BSCAddress != 'boolean')
        return res.status(200).json({ status: false, message: `BSCAddress status is not correct` })
    handleData({ Airdrop_Mission, Airdrop_Email, Airdrop_BSCAddress: mission_data })
    return res.status(200).json({ status: true, message: ` data is processing` })
}

const handleData = async ({ Airdrop_Mission, Airdrop_Email, Airdrop_BSCAddress }) => {
    try {
        var sw_data = await SwModel.find()
        var final_data = []
        Promise.all(sw_data.map(async (sw, index) => {
            var check_mission = Airdrop_Mission.find(data => data.Mission_Name == sw.Sw_Action)
            if (check_mission.length > 0) {
                var check_address_airdrop = await AirdropModel.findOne({ Airdrop_Email: sw.Sw_Email })
                if (check_address_airdrop) {
                    var find_email = final_data.find(data => data.Sw_Email == sw.Sw_Email)
                    if (find_email.length > 0 && find_email[0][Sw_Action] != undefined) {
                        find_email[0][Sw_Action] = sw.Sw_Details
                    } else {
                        if (isNaN(find_email[0][Sw_Action])) {
                            find_email[0][Sw_Action] = 2
                        }
                        else {
                            find_email[0][Sw_Action] += 1
                        }
                    }
                }
            }
        }))
    } catch (error) {
        console.log(error)
        json_data.push({ error: error.message })
    }
}

const filterData = async (req,res) =>{

}

module.exports = {selectData}