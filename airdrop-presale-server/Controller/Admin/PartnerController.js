const PartnerModel = require('../../Model/Admin/PartnerModel')

const {nonAccentVietnamese} = require('../../Hepper/NonAccentVietnamese')

const partnerCreate = async(req,res) =>{
    try {
        const { Partner_Name_en, Partner_Name_vi, Partner_Description_en, Partner_Description_vi, Partner_Img, Partner_Link, Partner_Target, Partner_Show_en, Partner_Order, Partner_Created, Partner_Updated } = req.body
        var data = { Partner_Name_en, Partner_Name_vi, Partner_Description_en, Partner_Description_vi, Partner_Img, Partner_Link, Partner_Target, Partner_Show_en, Partner_Order, Partner_Created, Partner_Updated }
        // check admin
        if (req.user.User_RootAdmin != 1) return res.status(200).json({ status: false, message: "you are not admin" })
        // partner id
        var Partner_Lastest = await PartnerModel.find().sort({ Partner_ID: -1 }).limit(1)
        Partner_Lastest.length == 0 ? data.Partner_ID = 1 : data.Partner_ID = Partner_Lastest[0].Partner_ID + 1
        // change date fortmat
        data.Partner_Created = (new Date(Partner_Created)).getTime()
        data.Partner_Updated = (new Date(Partner_Updated)).getTime()
        // validate value
        var { errors } = await partnerValidate(data, 1)
        if(errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
            data.Partner_Name_en = await nonAccentVietnamese(Partner_Name_en)
        var test = await new PartnerModel(data).save()
        return res.status(200).json({status: false, message: "create partner success"})
    } catch (error) {
        console.log(error)
        return res.status(200).json({status: false, message: error})
    }
}
const partnerEdit = async () => {
    try {
        const { Partner_ID, Partner_Name_en, Partner_Description_en, Partner_Img, Partner_Link, Partner_Target, Partner_Show_en, Partner_Order, Partner_Created, Partner_Updated } = req.body
        var data = { Partner_Name_en, Partner_Name_vi, Partner_Description_en, Partner_Description_vi, Partner_Img, Partner_Link, Partner_Target, Partner_Show_en, Partner_Order, Partner_Created, Partner_Updated }
        var partner = await PartnerModel.findOne({ Partner_ID: Partner_ID , Partner_Deleted: false})
        if (!partner)
            return res.status(200).json({ status: false, message: `partner ${Partner_ID} not found` })
        var { errors } = await partnerValidate(data, 1)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        data.Partner_Created = (new Date(Partner_Created)).getTime()
        data.Partner_Updated = (new Date(Partner_Updated)).getTime()
        data.Partner_Name_en = await nonAccentVietnamese(Partner_Name_en)
        var test = await PartnerModel.updateOne({Partner_ID:Partner_ID},data)
        console.log(test)
        return res.status(200).json({ status: true, message: `update partner ${Partner_ID} success` })
    } catch (error) {
        return res.status(200).json({ status: false, message: error.message })
    }
}
const partnerDelete = async (req,res) => {
    var { Partner_ID } = req.body
    var message = []
    if (!Array.isArray(Partner_ID) || Partner_ID.length <= 0)
        return res.status(200).json({ status: false, message: "Partner_ID is not correct format." })
    for (var i = 0; i < Partner_ID.length; i++) {
        if(!isNaN(Partner_ID[i]))
        var Partner = await PartnerModel.findOne({ Partner_ID: Partner_ID[i], Partner_Deleted: false })
        if (!Partner)
            message.push({ status: false, message: `ID ${Partner_ID[i]} is not found` })
        else {
            Partner.Partner_Deleted = true
            Partner.save()
            message.push({ status: true, message: `Delete ID ${Partner_ID[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}
const partnerFilter = async(req,res) =>{
    try {
        var list_data = []
        var perPage = 5; // số lượng sản phẩm xuất hiện trên 1 page
        var page = req.body.page || 1;
        await PartnerModel.find({ Partner_Deleted: false }).select("-_id -createdAt -updatedAt")
            .skip((perPage * page) - perPage).limit(perPage)
            .exec(async (err, data) => {
                list_data = data
                var total_data = await PartnerModel.find({ Partner_Deleted: false }).countDocuments();
                return res.status(200).json({ status: true, data_page: data.length, data: list_data, perPage: perPage, page: page, total_data: total_data })
            });
    } catch (error) {
        return {status: false, message: error.message}
    }
}
const partnerDetail = async (req, res) => {
    var { Partner_ID } = req.params
    var partner = await PartnerModel.findOne({ Partner_ID: Partner_ID , Partner_Deleted: false})
    if (!partner)
        return res.status(200).json({ status: false, message: "partner is not found" })
    return res.status(200).json({ status: true, data: partner })
}

const partnerValidate = async (data, action) => {
    var errors = []
    if (data.Partner_Name_en == "" || data.Partner_Name_en == null || data.Partner_Name_en == undefined)
        errors.push({ field: "Partner_Name_en", error: "Partner_Name_en is not required" })
    if (data.Partner_Show_en == "" || data.Partner_Show_en == null || data.Partner_Show_en ==undefined)
        errors.push({ field: "Partner_Show_en", error: "Partner_Show_en is not required" })
    if (isNaN(parseInt(data.Partner_Order)))
        errors.push({ field: "Partner_Order", error: "Partner_Order is not correct fortmat" })
    return { errors: errors }
}

module.exports = { partnerCreate, partnerEdit, partnerDelete, partnerFilter, partnerDetail }
