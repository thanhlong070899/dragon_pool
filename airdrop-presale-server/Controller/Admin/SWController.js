const SwModel = require('../../Model/Admin/SWModel')

const {readFileCsv} = require('../../Hepper/ReadFileCsv')
const moment = require("moment")
const FULL_DATE_FORMAT = "YYYY-MM-DD"
const swUploadExcel = async (req,res) =>{
    var list_data = []

    try {
        // await readFileCsv()
        var { data } = req.body
        var lastest_sw = await SwModel.find().sort({ Sw_ID: -1 }).limit(1)
        await Promise.all(data.map(async (sw, index,) => {
            lastest_sw.length > 0 ? sw.Sw_ID = lastest_sw[0].Sw_ID + index + 1 : sw.Sw_ID = 1
            var { errors } = await swValidate(sw)
            if (errors > 0)
                list_data.push({ index: index, status: false, message: errors })
            else {
                try {
                    if(sw.Sw_Birthday)
                    sw.Sw_Birthday = moment.utc(new Date(sw.Sw_Birthday)).format(FULL_DATE_FORMAT)
                    sw.Sw_Created = new Date(Sw_Created).getTime()
                    var test = await new SwModel(sw).save()
                    await list_data.push({ index: index, status: true, message: `import data ${sw.Sw_ID} success` })
                } catch (error) {
                    list_data.push({ index: index, status: false, message: error.message })
                }
            }
        }))
        return res.status(200).json({ status: true, data: list_data })
    } catch (error) {
        list_data.push({ status: false, message: error.message })
    }
}

const swEdit = async (req,res) =>{
    try {
        const { Sw_ID, Sw_Name, Sw_Email, Sw_Birthday, Sw_EntryType, Sw_Action, Sw_Details, Sw_EntriesWorth, Sw_Created, Sw_Country, Sw_Status } = req.body
        var data = { Sw_ID, Sw_Name, Sw_Email, Sw_Birthday, Sw_EntryType, Sw_Action, Sw_Details, Sw_EntriesWorth, Sw_Created, Sw_Country, Sw_Status }
        if (Sw_ID == undefined || Sw_ID == "" || Sw_ID == null)
            return res.status(200).json({ status: false, message: "Sw_ID is required." })
        var sw = await SwModel.findOne({ Sw_ID: Sw_ID })
        if (!sw)
            return res.status(200).json({ status: false, message: `Sw ${Sw_ID} is not found.` })
        data.Sw_Created = new Date(data.Sw_Created).getTime()
        if (data.Sw_Birthday != undefined && data.Sw_Birthday != null && data.Sw_Birthday != "")
            data.Sw_Birthday = moment.utc(new Date(data.Sw_Birthday)).format(FULL_DATE_FORMAT)
        await SwModel.updateOne({Sw_ID:Sw_ID},data)
        return res.status(200).json({ status: true, message:`Edit sweep ${Sw_ID} success` })
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }
}
const swDelete = async (req, res) => {
    const { Sw_ID } = req.body
    var message = []
    if (!Array.isArray(Sw_ID) || Sw_ID.length <= 0)
        return res.status(200).json({ status: false, message: "Sw_ID is not correct format." })
    for (var i = 0; i < Sw_ID.length; i++) {
        if(!isNaN(Sw_ID[i]))
        var Sw = await SwModel.findOne({ Sw_ID: Sw_ID[i], Sw_Deleted: false })
        if (!Sw)
            message.push({ status: false, message: `ID ${Sw_ID[i]} is not found` })
        else {
            Sw.Sw_Deleted = true
            Sw.save()
            message.push({ status: true, message: `Delete ID ${Sw_ID[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}
const swFilter = async (req, res) => {
    try {
        var list_data = []
        var perPage = 20; // số lượng sản phẩm xuất hiện trên 1 page
        var page = req.body.page || 1;
        await SwModel.find({ Sw_Deleted: false }).select("-_id -createAt -updateAt")
            .skip((perPage * page) - perPage).limit(perPage)
            .exec( async (err, data) => {
                list_data = data
                var total_data = await SwModel.find({ Sw_Deleted: false }).countDocuments();
                return res.status(200).json({ status: true, data_page: data.length, data: list_data, perPage: perPage, page: page, total_data: total_data  })
            });

    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }
}
const swDetail = async (req,res) =>{
    var { Sw_ID } = req.params
    var sw = await SwModel.findOne({ Sw_ID: Sw_ID, Sw_Deleted: false }).select("-_id -createAt -updateAt")
    if (!sw)
        return res.status(200).json({ status: false, message: "Sw is not found" })
    return res.status(200).json({ status: true, data: sw })
}
const swValidate = async (data) =>{
    try {
        var errors = []
        if(data.Sw_Status && data.Sw_Status != true && data.Sw_Status != false)
            errors.push({ field: "Sw_Status", error: "Sw_Status is not correct format" })
            return {errors: errors}
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }
}

const swListType = async(req,res) =>{
    try {
        var dataEnum = await SwModel.schema.path('Sw_EntryType').enumValues
        return res.status(200).json({status: true, data: dataEnum})
    } catch (error) {
        res.status(200).json({status: false, message: error.message})
    }
}

module.exports = {swUploadExcel,swEdit,swDelete,swFilter,swDetail, swListType}