const FaqModel = require('../../Model/Admin/FaqModel')
const moment = require("moment")
const FULL_DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"

const faqCreate = async (req, res) => {
    try {
        console.log(moment.utc(Date.now()).format(FULL_DATE_FORMAT))
        const { Faq_Order, Faq_Title, Faq_Content } = req.body
        var data = { Faq_Order, Faq_Title, Faq_Content }
        const faq_lastest = await FaqModel.find().sort({ Faq_ID: -1 }).limit(1)
        faq_lastest.length > 0 ? data.Faq_ID = faq_lastest[0].Faq_ID + 1 : data.Faq_ID = 1
        const { errors } = await faqValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        var test = await new FaqModel(data).save()
        return res.status(200).json({ status: true, message: "create Faq success" })
    } catch (error) {
        console.log(error)
        return { status: false, message: error.message }
    }
}
const faqEdit = async (req, res) => {
    try {
        const { Faq_ID, Faq_Order, Faq_Title, Faq_Content } = req.body
        var data = { Faq_Order, Faq_Title, Faq_Content }
        if (!Faq_ID)
            return res.status(200).json({ status: false, message: "Faq is requied." })
        var faq = await FaqModel.findOne({ Faq_ID: Faq_ID, Faq_Deleted: false })
        if (!faq)
            res.status(200).json({ status: false, message: `Faq ${Faq_ID} is not found.` })
        const { errors } = await faqValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        var test = await FaqModel.updateOne({ Faq_ID: Faq_ID }, data)
        return res.status(200).json({ status: true, message: `Update faq ${Faq_ID} success.` })
    } catch (error) {
        console.log(error)
        return { status: false, message: error.message }
    }
}
const faqDelete = async (req, res) => {
    const { Faq_ID } = req.body
    var message = []
    if (!Array.isArray(Faq_ID) || Faq_ID.length <= 0)
        return res.status(200).json({ status: false, message: "Faq_ID is not correct format." })
    for (var i = 0; i < Faq_ID.length; i++) {
        if(!isNaN(Faq_ID[i]))
        var faq = await FaqModel.findOne({ Faq_ID: Faq_ID[i], Faq_Deleted: false })
        if (!faq)
            message.push({ status: false, message: `ID ${Faq_ID[i]} is not found` })
        else {
            faq.Faq_Deleted = true
            faq.save()
            message.push({ status: true, message: `Delete ID ${Faq_ID[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}
const faqFilter = async (req, res) => {
    try {
        var list_data = []
        var perPage = 5; // số lượng sản phẩm xuất hiện trên 1 page
        var page = req.body.page || 1;
        await FaqModel.find({ Faq_Deleted: false }).select("-_id -createdAt -updatedAt -Faq_Deleted")
            .skip((perPage * page) - perPage).limit(perPage)
            .exec(async (err, data) => {
                list_data = data
                var total_data = await FaqModel.find({ Faq_Deleted: false }).countDocuments();
                return res.status(200).json({ status: true, data_page: data.length, data: list_data, perPage: perPage, page: page, total_data: total_data })
            });
    } catch (error) {
        console.log(error)
        return { status: false, message: error.message }
    }
}
const faqDetail = async (req, res) => {
    var { Faq_ID } = req.params
    var faq = await FaqModel.findOne({ Faq_ID: Faq_ID, Faq_Deleted: false }).select("-_id -createAt -updateAt -Faq_Deleted")
    if (!faq)
        return res.status(200).json({ status: false, message: "Faq is not found" })
    return res.status(200).json({ status: true, data: faq })
}

const faqValidate = async (data) => {
    var errors = []
    if (data.Faq_Title == undefined || data.Faq_Title == "" || data.Faq_Title == null)
        errors.push({ field: "Faq_Title", error: "Faq_Title is required" })
    if (data.Faq_Content == undefined || data.Faq_Content == "" || data.Faq_Content == null)
        errors.push({ field: "Faq_Content", error: "Faq_Content is required" })
    if ((data.Faq_Order != "" || data.Faq_Order != null) && isNaN(data.Faq_Order))
        errors.push({ field: "Faq_Order", error: "Faq_Order is required" })
    return { errors: errors }
}

module.exports = { faqCreate, faqEdit, faqDelete, faqFilter, faqDetail }