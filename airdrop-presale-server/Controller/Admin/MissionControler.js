const MissionModel = require('../../Model/Admin/MissionModel')

const createMission = async (req, res) => {
    const { Mission_Name, Mission_Decription } = req.body
    var data = { Mission_Name, Mission_Decription }
    if (Mission_Name == undefined || Mission_Name == null || Mission_Name == "")
        return res.status(200).json({ status: false, message: "Mission name is required." })
    var lastest_mission = await MissionModel.find().sort({ Mission_ID: -1 }).limit(1)
    lastest_mission.length == 0 ? data.Mission_ID = 1 : data.Mission_ID = lastest_mission[0].Mission_ID + 1
    await new MissionModel(data).save()
    return res.status(200).json({ status: true, message: "Create mission success" })
}

const editMission = async (req, res) => {
    const { Mission_ID, Mission_Name, Mission_Decription } = req.body
    var data = { Mission_Name, Mission_Decription }
    if (isNaN(Mission_ID))
        return res.status(200).json({ status: false, message: "Mission ID is not correct" })
    if (Mission_Name == undefined || Mission_Name == null || Mission_Name == "")
        return res.status(200).json({ status: false, message: "Mission name is required." })
    var mission = await MissionModel.findOne({ Mission_ID: Mission_ID })
    if (!mission)
        return res.status(200).json({ status: false, message: `Mission ${Mission_ID} is not found` })
    await MissionModel.updateOne({ Mission_ID: Mission_ID }, data)
    return res.status(200).json({ status: true, message: `Update mission ${Mission_ID} success` })
}

const deleteMission = async (req,res) => {
    const { Mission_ID } = req.body
    var message = []
    if (!Array.isArray(Mission_ID) || Mission_ID.length <= 0)
        return res.status(200).json({ status: false, message: "Mission_ID is not correct format." })
    for (var i = 0; i < Mission_ID.length; i++) {
        if (!isNaN(Mission_ID[i]))
            var mission = await MissionModel.findOne({ Mission_ID: Mission_ID[i], Mission_Deleted: false })
        if (!mission)
            message.push({ status: false, message: `ID ${Mission_ID[i]} is not found` })
        else {
            mission.Mission_Deleted = true
            mission.save()
            message.push({ status: true, message: `Delete ID ${Mission_ID[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}

const filterMission = async(req,res) =>{
    try {
        var list_data = []
        var perPage = 5; // số lượng sản phẩm xuất hiện trên 1 page
        var page = req.body.page || 1;
        await MissionModel.find({ Mission_Deleted: false }).select("-_id -createdAt -updatedAt -Mission_Deleted -__v")
            .skip((perPage * page) - perPage).limit(perPage)
            .exec(async (err, data) => {z
                list_data = data
                var total_data = await MissionModel.find({ Mission_Deleted: false }).countDocuments();
                return res.status(200).json({ status: true, data_page: data.length, data: list_data, perPage: perPage, page: page, total_data: total_data })
            });
    } catch (error) {
        console.log(error)
        return { status: false, message: error.message }
    }
}

const detailMission = async(req,res) =>{
    var { Mission_ID } = req.params
    var Mission = await MissionModel.findOne({ Mission_ID: Mission_ID, Mission_Deleted: false }).select("-_id -createAt -updateAt -Mission_Deleted -__v")
    if (!Mission)
        return res.status(200).json({ status: false, message: `Mission ${Mission_ID} is not found` })
    return res.status(200).json({ status: true, data: Mission })
}

module.exports = { createMission, editMission, deleteMission, filterMission, detailMission }