const UserModel = require('../../Model/Auth/UserModel')
const PermissionModel = require('../../Model/Admin/PermissionModel')
const validator = require("email-validator");
const { encrypt } = require('../../Hepper/Xxtea')
// const { createSetting } = require('./SettingController')

const userCreate = async (req, res) => {
    try {
        var { User_Avatar, User_DocumentPhotos, User_Name, User_FirstName, User_LastName, User_Gender, User_Birthday, User_Country, User_Email, User_Mobile, User_Address, User_BSCAddress, User_Group, User_Permissions, User_Registered, User_VipExpirationDate, User_Status, User_Salt } = req.body
        var data = { User_Avatar, User_DocumentPhotos, User_Name, User_FirstName, User_LastName, User_Gender, User_Birthday, User_Country, User_Email, User_Mobile, User_Address, User_BSCAddress, User_Group, User_Permissions, User_Registered, User_VipExpirationDate, User_Status, User_Salt }
        var user = await UserModel.findOne({ User_ID: req.user.User_ID })
        if (!user) return res.status(200).json({ status: false, message: "user is not found" })
        if (user.User_RootAdmin != 1) return res.status(200).json({ status: false, message: "you are not admin" })
        const { errors } = await validateUser(data, 1, user)
        if (errors.length > 0) return res.status(200).json({ status: false, data: errors })
        // save user

        var list_user = await UserModel.find().sort({ User_ID: -1 }).limit(1)
        data.User_Registered =  new Date(data.User_Registered).getTime()
        data.User_VipExpirationDate = new Date(data.User_VipExpirationDate).getTime()
        data.User_Password = (await encrypt("123456")).data
        data.User_ID = list_user[0].User_ID + 1
        // var setting = await createSetting(data)
        if (!setting.status) {
            return res.status(200).json({ status: false, data: setting.message })
        }

        var test = await new UserModel(data).save()
        return res.status(200).json({ status: true, message: "create user success" })
    } catch (error) {
        console.log(error)
        return { status: false, message: error.message }
    }
}

const userDelete = async (req, res) => {
    const { User_ID } = req.body
    var message = []
    if (!Array.isArray(User_ID) || User_ID.length <= 0)
        return res.status(200).json({ status: false, message: "User_ID is not correct format." })
    for (var i = 0; i < User_ID.length; i++) {
        if(!isNaN(User_ID[i]))
        var User = await UserModel.findOne({ User_ID: User_ID[i], User_Deleted: false })
        if (!User)
            message.push({ status: false, message: `ID ${User_ID[i]} is not found` })
        else {
            User.User_Deleted = true
            User.save()
            message.push({ status: true, message: `Delete ID ${User_ID[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}

const userGetall = async(req,res)=>{
    var data  = await UserModel.find().select("-_id -createdAt -updatedAt -User_Password -User_Token -User_SessionIDs")
    return res.status(200).json({status: true, data: data})
}

const userEdit = async (req, res) => {
    try {
        var { User_ID, User_Avatar, User_DocumentPhotos, User_Name, User_FirstName, User_LastName, User_Gender, User_Birthday, User_Country, User_Email, User_EmailVerified, User_Mobile, User_MobileVerified, User_Address, User_BSCAddress, User_Group, User_Permissions, User_Registered, User_VipExpirationDate, User_Status, User_Salt } = req.body
        var data = { User_Avatar, User_DocumentPhotos, User_Name, User_FirstName, User_LastName, User_Gender, User_Birthday, User_Country, User_Email, User_EmailVerified, User_Mobile, User_MobileVerified, User_Address, User_BSCAddress, User_Group, User_Permissions, User_Registered, User_VipExpirationDate, User_Status, User_Salt }
        // check admin
        if (req.user.User_RootAdmin != 1) return res.status(200).json({ status: false, message: "you are not admin" })
        // get user edit
        if (User_ID == "" || User_ID == null || User_ID == undefined) return res.status(200).json({ status: false, message: "input User_ID please" })
        var user = await UserModel.findOne({ User_ID: User_ID })
        if (!user) return res.status(200).json({ status: false, message: "user is not found" })
        const { errors } = await validateUser(data, 0, user)
        if (errors.length > 0) return res.status(200).json({ status: false, data: errors })
        // save user
        data.User_Registered = new Date(data.User_Registered).getTime()
        data.User_VipExpirationDate = new Date(data.User_VipExpirationDate).getTime()
        await UserModel.updateOne({User_ID:User_ID},data)
        return res.status(200).json({ status: true, message: `update user ${User_ID} success` })
    } catch (error) {
        console.log(error)
        return { status: false, message: error.message }
    }

}

const userFilter = async (req, res) => {
    var list_data = []
    var perPage = 5; // số lượng sản phẩm xuất hiện trên 1 page
    var page = req.body.page || 1;
    await UserModel.find({ User_Deleted: false }).select("-_id -createdAt -updatedAt -User_Password -User_Token -User_SessionIDs")
        .skip((perPage * page) - perPage).limit(perPage)
        .exec(async (err, data) => {
            list_data = data
            var total_data = await UserModel.find({ User_Deleted: false }).countDocuments();
            return res.status(200).json({ status: true, data_page: data.length, data: list_data , perPage: perPage, page: page, total_data: total_data })
        });
}

const userDetail = async (req, res) => {
    try {
        const { User_ID } = req.params
        if (User_ID == "" || User_ID == null || User_ID == undefined)
            return res.status(200).json({ status: false, message: "input User_ID please" })
        var user = await UserModel.findOne({ User_ID: User_ID, User_Deleted: false }).select("-_id -createdAt -updatedAt -User_Password -User_Token -User_SessionIDs")
        if (!user)
            return res.status(200).json({ status: false, message: "User is not found" })
        return res.status(200).json({ status: true, data: user })
    } catch (error) {
        console.log(error)
    }
}

const userInfo = async (req, res) =>{
    try {
        var user = await UserModel.findOne({ User_ID: req.user.User_ID}).select("-_id -createdAt -updatedAt -User_Password -User_Token -User_SessionIDs")
        return res.status(200).json({ status: true, data: user })
    } catch (error) {
        return {status: false, message: error.message}
    }
}

const validateUser = async (data, action, user) => {

    // action = 1 => create, 0 => edit
    var errors = []

    if (data.User_Permissions != undefined && data.User_Permissions != null && data.User_Permissions.length > 0) {
        for (var i = 0; i < data.User_Permissions.length; i++) {
            var check = await UserModel.findOne({ Permission_Name: data.User_Permissions[i] })
            if (!check)
                errors.push({ field: "User_Permissions", error: `User_Permissions ${data.User_Permissions[i]} is not found` })
        }
    }
    // check email
    if (!validator.validate(data.User_Email))
        errors.push({ field: "User_Email", error: "User_Email is not correct" })
    if (action == 1)
        var existed_email = await UserModel.findOne({ User_Email: data.User_Email })
    else
        var existed_email = await UserModel.findOne({ User_Email: data.User_Email, User_ID: { $ne: user.User_ID } })
    if (existed_email)
        errors.push({ field: "User_Email", error: "User_Email is existed" })
    if (data.User_FirstName == "" || data.User_FirstName == null || data.User_FirstName == undefined)
        errors.push({ field: "User_FirstName", error: "User_FirstName is required" })
    if (data.User_LastName == "" || data.User_LastName == null || data.User_LastName == undefined)
        errors.push({ field: "User_LastName", error: "User_LastName is required" })
    if (typeof parseInt(data.User_Gender) != 'number')
        errors.push({ field: "User_Gender", error: "User_Gender is not correct" })
    if (data.User_BSCAddress == "" || data.User_BSCAddress == null || data.User_BSCAddress == undefined)
        errors.push({ field: "User_BSCAddress", error: "User_BSCAddress is required" })
    if (data.User_VipExpirationDate == "" || data.User_VipExpirationDate == null || data.User_VipExpirationDate == undefined)
        errors.push({ field: "User_VipExpirationDate", error: "User_VipExpirationDate is required" })
    if (data.User_Status != false && data.User_Status != true)
        errors.push({ field: "User_Status", error: "User_Status is required" })
    if (data.User_Salt == "" || data.User_Salt == null || data.User_Salt == undefined)
        errors.push({ field: "User_Salt", error: "User_Salt is required" })
    return { errors: errors }
}


module.exports = { userCreate, userDelete, userEdit, userFilter, userDetail ,userInfo, userGetall}