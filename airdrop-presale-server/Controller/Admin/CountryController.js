const CountryModel = require('../../Model/Admin/CountryModel')

const listCountry = async (req,res) =>{
    try {
        var country = await CountryModel.find().select('-_id -createdAt -updatedAt')
        return res.status(200).json({status: true, data: country})
    } catch (error) {
        return res.status(200).json({status: false , message: error.message})
    }
}

module.exports = {listCountry}