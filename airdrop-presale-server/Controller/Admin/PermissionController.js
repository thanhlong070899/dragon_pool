const PermissionModel = require('../../Model/Admin/PermissionModel')

const listPermission = async (req,res) =>{
    try {
        const permission = await PermissionModel.find().select("-_id -Permission_Deleted")
        return res.status(200).json({status: true, data : permission})
    } catch (error) {
        return res.status(200).json({ status: false, message: error.message })
    }
}
// const createPermission = async() =>{
//     new PermissionModel({
//         Permission_ID :2,
//         Permission_Name:"Management HRM",
        
//     }).save()
//     //Management HRM
// }
// createPermission()
module.exports = {listPermission}