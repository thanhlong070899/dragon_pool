const MemberModel = require('../../Model/Admin/MemberModel')
const { nonAccentVietnamese } = require('../../Hepper/NonAccentVietnamese')
const moment = require("moment")
const FULL_DATE_FORMAT = "YYYY-MM-DD HH:mm:ss"

const memberCreate = async (req, res) => {
    try {
        const { Member_Name_en, Member_Name_vi, Member_Content_en, Member_Avatar, Member_Priority, Member_Show_en } = req.body
        var data = { Member_Name_en, Member_Name_vi, Member_Content_en, Member_Avatar, Member_Priority, Member_Show_en }
        if (req.user.User_RootAdmin != 1)
            return res.status(200).json({ status: false, message: "you are not admin" })
        var Member_Lastest = await MemberModel.find().sort({ Member_ID: -1 }).limit(1)
        Member_Lastest.length == 0 ? data.Member_ID = 1 : data.Member_ID = Member_Lastest[0].Member_ID + 1
        var { errors } = await memberValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        data.Member_Name_en = await nonAccentVietnamese(Member_Name_en)
        var test = await new MemberModel(data).save()
        return res.status(200).json({ status: true, message: "create customer success" })
    } catch (error) {
        return res.status(200).json({ status: false, message: error.message })
    }
}

const memberEdit = async (req, res) => {
    try {
        const { Member_ID, Member_Name_en, Member_Name_vi, Member_Content_en, Member_Avatar, Member_Priority, Member_Show_en } = req.body
        var data = { Member_Name_en, Member_Name_vi, Member_Content_en, Member_Avatar, Member_Priority, Member_Show_en }
        if (req.user.User_RootAdmin != 1)
            return res.status(200).json({ status: false, message: "you are not admin" })
        if (Member_ID == undefined || Member_ID == "" || Member_ID == null)
            return res.status(200).json({ status: false, message: "Member_ID is required" })
        var member = await MemberModel.findOne({ Member_ID: Member_ID, Member_Deleted: false })
        if (!member)
            return res.status(200).json({ status: false, message: `Member ${Member_ID} is not found` })
        var { errors } = await memberValidate(data)
        if (errors.length > 0)
            return res.status(200).json({ status: false, message: errors })
        data.Member_Name_en = await nonAccentVietnamese(Member_Name_en)
        var test = await MemberModel.updateOne({Member_ID:Member_ID},data)
        return res.status(200).json({ status: true, message: `Update member ${Member_ID} success` })
    } catch (error) {
        return res.status(200).json({ status: false, message: error.message })
    }
}
const memberDelete = async (req, res) => {
    const { Member_ID } = req.body
    var message = []
    if (!Array.isArray(Member_ID) || Member_ID.length <= 0)
        return res.status(200).json({ status: false, message: "Member_ID is not correct format." })
    for (var i = 0; i < Member_ID.length; i++) {
        if(!isNaN(Member_ID[i]))
        var member = await MemberModel.findOne({ Member_ID: Member_ID[i], Member_Deleted: false })
        if (!member)
            message.push({ status: false, message: `ID ${Member_ID[i]} is not found` })
        else {
            member.Member_Deleted = true
            member.save()
            message.push({ status: true, message: `Delete ID ${Member_ID[i]} success` })
        }
    }
    return res.status(200).json({ status: true, message: message })
}
const memberFilter = async (req, res) => {
    try {
        var list_data = []
        var perPage = 5; // số lượng sản phẩm xuất hiện trên 1 page
        var page = req.body.page || 1;
        await MemberModel.find({ Member_Deleted: false }).select("-_id -createdAt -updatedAt")
            .skip((perPage * page) - perPage).limit(perPage)
            .exec(async (err, data) => {
                list_data = data
                var total_data = await MemberModel.find({ Member_Deleted: false }).countDocuments();
                return res.status(200).json({ status: true, data_page: data.length, data: list_data, perPage: perPage, page: page, total_data: total_data })
            });
    } catch (error) {
        return { status: false, message: error.message }
    }
}
const memberDetail = async (req, res) => {
    var { Member_ID } = req.params
    var member = await MemberModel.findOne({ Member_ID: Member_ID, Member_Deleted: false }).select("-_id -createdAt -updatedAt")
    if (!member)
        return res.status(200).json({ status: false, message: "Member is not found" })
    return res.status(200).json({ status: true, data: member })
}
const memberValidate = async (data) => {
    var errors = []
    if (data.Member_Name_en == undefined || data.Member_Name_en == "" || data.Member_Name_en == null)
        errors.push({ field: "Member_Name_en", error: "Member_Name_en is required" })
    if (isNaN(parseFloat(data.Member_Priority)))
        errors.push({ field: "Member_Priority", error: "Member_Priority is not correct format" })
    return { errors: errors }
}

module.exports = { memberCreate, memberEdit, memberDelete, memberFilter, memberDetail }