const xxtea = require('xxtea-node')


exports.encrypt = function (str_data) {
    try {
        const encrypted_bytes = xxtea.encryptToString(str_data, process.env.KEY_HASH);
        return { status: true, data: encrypted_bytes }
    } catch (error) {
        return { status: false, message: error.message }
    }
}

exports.decrypt = function (encrypted_data) {
    try {
        const decrypted_str = xxtea.decryptToString(encrypted_data, process.env.KEY_HASH);
        return { status: true, data: decrypted_str }
    } catch (error) {
        return { status: false, message: error.message }
    }
}
