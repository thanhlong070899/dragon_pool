const csv = require('csv-parser')
const fs = require('fs')


const readFileCsv = async () => {
    const results = []
    fs.createReadStream('Config/sw.csv')
        .pipe(csv())
        .on('data', (data) => results.push(data))
        .on('end', () => {
            console.log(results);
        });
}

module.exports = { readFileCsv }