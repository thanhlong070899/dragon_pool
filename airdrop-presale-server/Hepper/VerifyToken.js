const vertifyToken = (token, bearerHeader)=>{
    if( typeof bearerHeader !== 'undefined'){
        const bearerToken = bearerHeader.split(' ')[1]
        token = bearerToken
        return {status:true , token:token}
    }else{
       return false
    }
}

const login = async (req,res,next) =>{
    const bearerHeader = req.headers['authorization']
    var status =  await vertifyToken(req.token, bearerHeader)
    if(status.status)
    {
        req.token = status.token
        next()
    }
    else
    res.sendStatus(403)
}

const isAdmin = async (req,res,next) =>{
    
}

 module.exports = {login}