const  randomstring = require("randomstring");

const randomString = async () => {
    try {
        var data = randomstring.generate({
            length: 10,
            charset: 'alphanumeric'
        });

        return { status: true, data: data }
    } catch (error) {
        return { status: false, error: error.message }
    }
}

module.exports = { randomString }