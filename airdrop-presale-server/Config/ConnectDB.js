const mongoose = require('mongoose')

const admin_DB = mongoose.createConnection(process.env.DATABASE_ADMIN, { useNewUrlParser: true }, (error) => {
    if (!error) {
        console.log("connect database admin success");
    }
    else
        console.log("connect database admin failed");
}, 5000)

module.exports = {admin_DB}