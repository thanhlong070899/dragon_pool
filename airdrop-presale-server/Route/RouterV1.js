const router = require("express").Router()


const adminSettingRouter = require('./Admin/SettingRouter')
const adminUserRouter = require('./Admin/UserRouter')
const adminAuthRouter = require('./Auth/Auth')
const adminPartnerRouter = require('./Admin/PartnerRouter')
const adminMemberRouter = require('./Admin/MemberRouter')
const adminFaqRouter = require('./Admin/FaqRouter')
const adminSWRouter = require('./Admin/SWRouter')
const adminPostRouter = require('./Admin/PostRouter')
const adminPermission = require('./Admin/PermissionRouter')
const CountryRouter = require('./Admin/CountryRouter')
const MissionRouter = require('./Admin/MissionRouter')

// auth
router.use('/admin/auth', adminAuthRouter)


// admin
router.use('/admin/setting', adminSettingRouter)
router.use('/admin/user', adminUserRouter)
router.use('/admin/member', adminMemberRouter)
router.use('/admin/partner', adminPartnerRouter)
router.use('/admin/faq', adminFaqRouter)
router.use('/admin/sw', adminSWRouter)
router.use('/admin/post', adminPostRouter)
router.use('/admin/permission', adminPermission)
router.use('/admin/country', CountryRouter)
router.use('/admin/mission', MissionRouter)


module.exports = router