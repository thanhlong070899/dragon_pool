const router = require("express").Router()
const AuthController = require('../../Controller/Auth/AuthController')
const {validateJWT} = require('../../Middleware/AuthHandle')


router.post('/login', AuthController.userLogin)
router.get('/logout',validateJWT, AuthController.userLogout)
router.post('/changepassword',validateJWT, AuthController.userChangePassword)
router.post('/sendEmail', AuthController.sendEmail)
router.post('/verifyEmail', AuthController.verifyEmail)
router.post('/resetPassword', AuthController.resetPassword)


module.exports = router