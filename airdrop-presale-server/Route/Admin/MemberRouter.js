const router = require("express").Router()
const {validateJWTAdmin} = require('../../Middleware/AuthHandle')

const MemberController = require('../../Controller/Admin/MemberController')

router.post('/create', validateJWTAdmin, MemberController.memberCreate)
router.post('/edit', validateJWTAdmin, MemberController.memberEdit)
router.post('/delete', validateJWTAdmin, MemberController.memberDelete)
router.post('/filter', validateJWTAdmin, MemberController.memberFilter)
router.get('/detail/:Member_ID', validateJWTAdmin, MemberController.memberDetail)

module.exports = router
