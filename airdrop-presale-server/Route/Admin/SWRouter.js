const router = require("express").Router()
const {validateJWTAdmin} = require('../../Middleware/AuthHandle')

const SWController = require('../../Controller/Admin/SWController')

router.post('/create', validateJWTAdmin, SWController.swUploadExcel)
router.post('/edit', validateJWTAdmin, SWController.swEdit)
router.post('/delete', validateJWTAdmin, SWController.swDelete)
router.post('/filter', validateJWTAdmin, SWController.swFilter)
router.get('/detail/:Sw_ID', validateJWTAdmin, SWController.swDetail)
router.get('/listType', validateJWTAdmin, SWController.swListType)

module.exports = router