const router = require("express").Router()
const {validateJWTAdmin} = require('../../Middleware/AuthHandle')

const CountryController = require('../../Controller/Admin/CountryController')

router.get('/filter', validateJWTAdmin, CountryController.listCountry)

module.exports = router