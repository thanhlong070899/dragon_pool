const router = require("express").Router()
const {validateJWTAdmin} = require('../../Middleware/AuthHandle')

const PostController = require('../../Controller/Admin/PostController')

router.post('/create', validateJWTAdmin, PostController.postCreate)
router.post('/edit', validateJWTAdmin, PostController.postEdit)
router.post('/delete', validateJWTAdmin, PostController.postDelete)
router.post('/filter', validateJWTAdmin, PostController.postFilter)
router.get('/detail/:Post_ID', validateJWTAdmin, PostController.postDetail)

module.exports = router