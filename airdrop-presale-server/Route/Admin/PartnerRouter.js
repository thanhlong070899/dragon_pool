const router = require("express").Router()
const {validateJWT} = require('../../Middleware/AuthHandle')

const PartnerController = require('../../Controller/Admin/PartnerController')

router.post('/create', validateJWT, PartnerController.partnerCreate)
router.post('/edit', validateJWT, PartnerController.partnerEdit)
router.post('/delete', validateJWT, PartnerController.partnerDelete)
router.post('/filter', validateJWT, PartnerController.partnerFilter)
router.get('/detail/:Partner_ID', validateJWT, PartnerController.partnerDetail)

module.exports = router
