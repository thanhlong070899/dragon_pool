const router = require("express").Router()
const {validateJWTAdmin} = require('../../Middleware/AuthHandle')


const { editSetting, loadSetting, createSetting, deleteSetting, detailSetting } = require('../../Controller/Admin/SettingController')

router.post('/create',validateJWTAdmin, createSetting)
router.post('/delete',validateJWTAdmin, deleteSetting)
router.post('/edit',validateJWTAdmin, editSetting)
router.post('/filter',validateJWTAdmin, loadSetting)
router.get('/detail/:Setting_Lang',validateJWTAdmin, detailSetting)

module.exports = router