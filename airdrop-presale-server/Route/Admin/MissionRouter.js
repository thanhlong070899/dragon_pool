const router = require("express").Router()
const { validateJWTAdmin } = require('../../Middleware/AuthHandle')

const MissionController = require('../../Controller/Admin/MissionControler')

router.post('/create', validateJWTAdmin, MissionController.createMission)
router.post('/edit', validateJWTAdmin, MissionController.editMission)
router.post('/delete', validateJWTAdmin, MissionController.deleteMission)
router.post('/filter', validateJWTAdmin, MissionController.filterMission)
router.get('/detail/:Mission_ID', validateJWTAdmin, MissionController.detailMission)

module.exports = router
