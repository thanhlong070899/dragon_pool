const router = require("express").Router()
const {validateJWTAdmin} = require('../../Middleware/AuthHandle')

const PermissionController = require('../../Controller/Admin/PermissionController')

router.get('/filter', validateJWTAdmin, PermissionController.listPermission)

module.exports = router
