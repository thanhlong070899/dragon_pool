const router = require("express").Router()

const {validateJWT} = require('../../Middleware/AuthHandle')

const UserController =  require('../../Controller/Admin/UserController')

router.post('/create', validateJWT, UserController.userCreate)
router.post('/delete', validateJWT, UserController.userDelete)
router.post('/edit', validateJWT, UserController.userEdit)
router.post('/filter', validateJWT, UserController.userFilter)
router.get('/detail/:User_ID', validateJWT, UserController.userDetail)
router.get('/info', validateJWT, UserController.userInfo)
router.get('/all', validateJWT, UserController.userGetall)

module.exports = router
