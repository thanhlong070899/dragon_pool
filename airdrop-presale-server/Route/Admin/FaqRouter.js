const router = require("express").Router()
const {validateJWTAdmin} = require('../../Middleware/AuthHandle')

const FaqController = require('../../Controller/Admin/FaqController')

router.post('/create', validateJWTAdmin, FaqController.faqCreate)
router.post('/edit', validateJWTAdmin, FaqController.faqEdit)
router.post('/delete', validateJWTAdmin, FaqController.faqDelete)
router.post('/filter', validateJWTAdmin, FaqController.faqFilter)
router.get('/detail/:Faq_ID', validateJWTAdmin, FaqController.faqDetail)

module.exports = router