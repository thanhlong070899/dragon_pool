require("dotenv").config({ path: "Config/.env" })
const express = require('express')
const cors = require('cors')
const app = express()
require('./Config/ConnectDB')
app.use(express.json())
app.use(cors())

app.use('/api/v1', require('./Route/RouterV1'))
app.get('/', (req, res, next) => {
    return res.send('test api.')
})

app.listen(process.env.PORT, () => console.log(`Server is running on port ${process.env.PORT}`))