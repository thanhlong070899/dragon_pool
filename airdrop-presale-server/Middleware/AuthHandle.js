const UserModel = require('../Model/Auth/UserModel')
const JWT = require('jsonwebtoken')

// Generate jwt token
exports.generateJWT = function(data) {
    try {
      const {JWT_SECRET_KEY, JWT_EXPIRES_IN} = process.env
      return JWT.sign(data, JWT_SECRET_KEY, {expiresIn: JWT_EXPIRES_IN})
    } catch (error) {
      throw error
    }
  }
  
  // Verify jwt token
  exports.verifyJWT = function(jwt_token) {
    try {
      const {JWT_SECRET_KEY} = process.env
      return JWT.verify(jwt_token, JWT_SECRET_KEY)
    } catch (error) {
      throw error
    }
  }
  
  // Validate the jwt token
  exports.validateJWT = async function(req, res, next) {
    const {authorization} = req.headers
    try {
      // No token
        if (!authorization) return res.status(200).json({ status: false, message: "No bearer token attached." })
  
      // Check format
      if (!authorization.startsWith('Bearer')) return res.status(200).json({ status: false, message: "Not a bearer token." })
  
      // Get token from header
      const jwt_token = authorization.split("Bearer ")[1]
      
      // Null token
      if (!jwt_token) return res.status(200).json({ status: false, message: "Invalid token." })
  
      // Verify token
      const {User_ID} = module.exports.verifyJWT(jwt_token)
      // Get user from token
      const user = await UserModel.findOne({ User_ID, jwt_token})
      // Not found user
      if (!user) return res.status(200).json({ status: false, message: "Invalid credentials." }) 
  
      // Assign to req
      req.user = user
  
      next()
    } catch (error) {
      error.statusCode = 200
      return next(error)
    }
  }
  
  exports.validateJWTAdmin = async function(req, res, next) {
    const {authorization} = req.headers
    try {
      // No token
        if (!authorization) return res.status(200).json({ status: false, message: "No bearer token attached." })
  
      // Check format
      if (!authorization.startsWith('Bearer')) return res.status(200).json({ status: false, message: "Not a bearer token." })
  
      // Get token from header
      const jwt_token = authorization.split("Bearer ")[1]
      
      // Null token
      if (!jwt_token) return res.status(200).json({ status: false, message: "Invalid token." })
  
      // Verify token
      const {User_ID} = module.exports.verifyJWT(jwt_token)
      
      // Get user from token
      const user = await UserModel.findOne({ User_ID, jwt_token })
      // Not found user
      if (!user) return res.status(200).json({ status: false, message: "Invalid credentials." }) 

      if (user.User_RootAdmin != 1)
        if (!user) return res.status(200).json({ status: false, message: "You are not admin." })

      // Assign to req
      req.user = user
  
      next()
    } catch (error) {
      error.statusCode = 200
      return next(error.message)
    }
  }