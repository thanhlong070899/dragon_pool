const WalletModel = require('../Model/WalletModel')
const { web3 } = require('../Function/web3')
const create = async (req, res) => {
    try {
        var { wallet, father, sign, signature } = req.body

        var check_wallet = await web3.utils.isAddress(wallet)
        var check_father = await web3.utils.isAddress(father)
        if (!check_wallet)
            return res.status(200).json({ status: false, message: "wallet is not correct", data: { registed: false } })
        wallet = wallet.toLowerCase()
        check_wallet = await WalletModel.findOne({ Address: wallet })

        if (check_wallet)
            return res.status(200).json({ status: true, message: "wallet is registed", data: { registed: true } })
        else if (!sign) {
            return res.status(200).json({ status: true, message: "", data: { sign: process.env.SIGN, registed: false } })
        }

        var data = await web3.eth.accounts.recover(process.env.SIGN, sign)

        if (data.toLowerCase() != wallet.toLowerCase()) {
            data = await web3.eth.accounts.recover(await web3.utils.toChecksumAddress(wallet), sign);//process.env.SIGN
            if (data.toLowerCase() != wallet.toLowerCase())
                return res.status(200).json({ status: false, message: "Sign is not correct", data: { registed: false } })
        }
        if (check_father) {
            father = father.toLowerCase()
            check_father = await WalletModel.findOne({ Address: father })
            if (!check_father)
                check_father = await WalletModel.findOne({ Address: "0xc9921b2ac1d04d2e1b5e65c56a17c13565918cda" })
        }
        else {
            check_father = await WalletModel.findOne({ Address: "0xc9921b2ac1d04d2e1b5e65c56a17c13565918cda" })
        }
        var ID
        var lastest_data = await WalletModel.find().sort({ ID: -1 }).limit(1)
        lastest_data.length > 0 ? ID = lastest_data[0].ID + 1 : ID = 1
        if (!check_father) {
            check_father = {}
            check_father.Wallet = []
        }
        check_father.Wallet.unshift(ID)
        await new WalletModel({
            ID: ID,
            Wallet: check_father.Wallet,
            Address: wallet
        }).save()
        return res.status(200).json({ status: true, message: "Registered success", data: { registed: false } })
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message, data: { registed: false } })
    }
}

const list = async (req, res) => {
    try {
        var { wallet, page, sort } = req.body
        // order 0 tìm tuyến trên , khác 0 tìm tuyến dưới 
        if (wallet != undefined) {
            wallet = wallet.toLowerCase()
        }
        var list_data = {}
        list_data.data = []
        var find_wallet = await WalletModel.findOne({ Address: wallet })
        if (sort == undefined)
            sort = { ID: 1 }
        if (!find_wallet)
            return res.status(200).json({ status: false, message: `Wallet ${wallet} is not found` })
        var query = { Wallet: { $in: find_wallet.Wallet[0] } }

        var perPage = 20; // số lượng sản phẩm xuất hiện trên 1 page
        var pages = page || 1;
        await WalletModel.find(query).select("-_id -createdAt -updatedAt -__v").sort(sort)
            // .skip((perPage * pages) - perPage).limit(perPage)
            .exec(async (err, data) => {
                await data.map(wallet => {
                    var index = wallet.Wallet.indexOf(find_wallet.ID)
                    if (index > 0)
                        list_data.data.push({ ID: wallet.ID, F: index, Address: wallet.Address })
                })
                var total_data = data.length
                list_data.total_data = total_data
                var final_data = await list_data.data.sort(function (a, b) { return a.F - b.F });
                final_data = final_data.slice((perPage * pages) - perPage, perPage * pages)
                return res.status(200).json({ status: true, data: final_data })
            });
    } catch (error) {
        console.log(error)
        return res.status(200).json({ status: false, message: error.message })
    }
}

function Fcsort(array) {
    array.sort(function (a, b) {
        console.log(b.F)
        return b.F - a.F;
    });
}
module.exports = { create, list }