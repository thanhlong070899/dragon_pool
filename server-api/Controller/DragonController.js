const DragonModel = require('../Model/DragonModel')

const {sleep, getRandomArbitrary} = require('../Function/Genaral')

const filterDragon = async (req,res) =>{
    try {
        var { sort, query, page, status_sort} = req.body
        var perPage = 20; // số lượng sản phẩm xuất hiện trên 1 page
        var pages = page || 1;
        // 0 => or , còn lại and
        if (status_sort != undefined && status_sort != 0)
            query = { $or: query }
        else
            query = { $and: query }
        await DragonModel.find(query).select("-_id -createdAt -updatedAt -__v").skip((perPage * pages) - perPage).limit(perPage).sort(sort).exec(async (error, data) => {
            if (error)
                return res.status(400).json({ status: false, message: error.message })
            var result = {}
            result.length = data.length
            result.total = await DragonModel.find(query).countDocuments()
            result.data = data
            console.log(data)
            return res.status(200).json({ status: true, message: "Find dragon success", data: result })
        })
    } catch (error) {
        console.log(error)
        return res.status(400).json({ status: false, message: error.message })
    }
}

const createDragon = async () =>{
    try {
        for(var index = 0 ; index < 300; index++)
        {
            await sleep(500)
            var lv = parseInt(await getRandomArbitrary(1, 10))
            var rank = parseInt(await getRandomArbitrary(1, 5))
            new DragonModel({
                Dragon_ID: index,
                Dragon_Image: '/Dragon/Image/' + index,
                Dragon_Name: 'Dragon '+ index,
                Dragon_Lv: lv,
                Dragon_Rank: rank
            }).save()
            console.log(index)
        }
    } catch (error) {
        console.log(error)
    }
}



module.exports = { filterDragon}