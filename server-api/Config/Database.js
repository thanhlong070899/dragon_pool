const mongoose = require('mongoose')

const admin_DB = mongoose.createConnection(process.env.DATABASE_DP, { useNewUrlParser: true }, (error) => {
    if (!error) {
        console.log(`connect database success`);
    }
    else
        console.log(`connect database failed`);
}, 5000)

module.exports = {admin_DB}