require("dotenv").config({ path: "./Config/.env" })
const express = require('express')
const cors = require('cors')
const app = express()
app.use(express.json())
app.use(cors())

require('./Config/Database')

app.get('/', (req,res) =>{
    res.send("test")
})

app.use('/api/v1', require('./Route/RouterV1'))

app.listen(process.env.PORT,() =>{console.log(`connect port ${process.env.PORT}`)})