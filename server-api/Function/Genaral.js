function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = { getRandomArbitrary, sleep }