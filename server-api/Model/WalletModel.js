const mongoose = require('mongoose')

const connect = require('../Config/Database')

const userSchema = new mongoose.Schema({
    ID: {
        type: Number,
        required: true,
        unique: [true, "User ID is taken."],
    },
    Wallet:{
        type: Array,
        default: []
    },
    Address:{
        type: String
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('wallet', userSchema)