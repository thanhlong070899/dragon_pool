const mongoose = require('mongoose')

const connect = require('../Config/Database')

const userSchema = new mongoose.Schema({
    Dragon_ID: {
        type: Number,
        required: true,
        unique: [true, "Dragon ID is taken."],
    },
    Dragon_Image: {
        type: String,
        default: ""
    },
    Dragon_Name: {
        type: String,
        required: [true, "Dragon name is required"]
    },
    Dragon_Lv:{
        type: Number,
        default: 0
    },
    Dragon_Rank:{
        type: Number,
        default: 1
    },
    Dragon_Win:{
        type: Number,
        default: 0
    },
    Dragon_Lose:{
        type: Number,
        default: 0
    }
}, { timestamps: true })

module.exports = connect.admin_DB.model('dragon', userSchema)