const router = require("express").Router()
const walletController = require('../Controller/WalletController')

router.post('/create', walletController.create)
router.post('/list', walletController.list)

router.use('/dragon',require('./Dragon'))

module.exports = router