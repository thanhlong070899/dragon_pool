const router = require("express").Router()
const dragonController = require('../Controller/DragonController')

router.post('/filter', dragonController.filterDragon)

module.exports = router